<?php

namespace App\Http\Controllers;

use App\Models\Dokter;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{



    public function index()
    {
        $dokters = Dokter::all();
        // $users = User::all();
        return view('profile/index', compact('dokters'));
    }

    public function edit(User $user)
    {
        // $polis = User::all();
        return view('profil/edit', compact('user'));
    }
}
