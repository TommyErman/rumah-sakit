<?php

namespace App\Http\Controllers;

use App\Models\Dokter;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::all();
        return view('profile/index', compact('user'));
    }


    public function edit_password()
    {
        // $dokters = Dokter::all();
        return view('profile/edit_password');
    }
    public function update_password(Request $request)
    {
        request()->validate([
            'old_password' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $currentPassword = auth()->user()->password;
        $old_password = request('old_password');

        if (Hash::check($old_password, $currentPassword)) {

            auth()->user()->update([
                'password' => bcrypt(request('password')),
            ]);
            return back()->with('error', 'your password change succesfully');
        } else {
            return back()->withErrors(['old_password' => 'make you fill your current password ']);
        }
    }
}
