@extends('layout/main')
@section('title','My Profile')
    

@section('breadcrumbs')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">My Profile</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Profile</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->

@endsection

@section('content')
     <!-- Main content -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-md">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$dokter->name}}</h3>

                <p class="text-muted text-center">{{$dokter->poli->name}}</p>

            

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Email</strong>

                <p class="text-muted">
               {{$dokter->user->email}}
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted">{{$dokter->alamat}}</p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Contact Personal</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{$dokter->nomor_telepon}}</span>
                 
                </p>
                <strong><i class="fas fa-pencil-alt mr-1"></i> Status</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{$dokter->user->role}}</span>
                 
                </p>

              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection






   

 
