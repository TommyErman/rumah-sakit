@extends('layout/main')
@section('title','My Profile')
    

@section('breadcrumbs')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">My Profile</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Profile</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->

@endsection

@section('content')
     <!-- Main content -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-md">
            <form action="{{route('profile')}}" method="get">
              @csrf
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{auth()->user()->name}}</h3>


            

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Name</strong>

                <p class="text-muted">
               {{-- {{$dokter->user->email}} --}}
               {{auth()->user()->name}}
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Email</strong>

                <p class="text-muted"></p>
                    {{auth()->user()->email}}
                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Password</strong>
                
                <p class="text-muted">
                
                  <a href="{{route('edit_password')}}" class="btn btn-primary">Edit Password</a>
                </p>

              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </form>
          </div>
          <!-- /.col -->
        
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection






   

 
